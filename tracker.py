import requests
import logging
from config_manager import ConfigManager

def custom_track(update,event,cardname=None):
    manager = ConfigManager.get_instance()
    try:
        if(update.message==None):
            chatid = 0
        else:
            chatid = update.message.chat_id
        if (cardname != None): 
            cardname = ' '.join([el[:1].upper() + el[1:] for el in cardname.split(' ')])
        requests.post(manager.config["tracking_url"]+"track",data={'command':event,'chatid':str(chatid),'card':cardname})
    except Exception as e:
        logging.error(e) 
