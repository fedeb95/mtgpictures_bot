from dbconn.conn_manager import ConnManager
import tracker
import commutils.queries as queries

def handle(bot,update,args):
    text = "Hi! I'm your new bot for Mtg card pictures and prices. You can start with @mtgpictures_bot followed by a card name"
    bot.send_message(chat_id=update.message.chat_id, text=text)
    conn = ConnManager.get_instance().get_conn()
    try:
        c = ConnManager.get_instance().get_cursor(conn)
        c.execute(queries.query_users_where_chat_id,[str(update.message.chat_id)])
        data = c.fetchall()
        if data == None or len(data) == 0:
            #tracker.track_first(update)
            tracker.custom_track(update,"start")
            c.execute(queries.query_insert_user,[str(update.message.chat_id)])
            ConnManager.get_instance().commit(conn)
    finally:
        ConnManager.get_instance().close(conn)





