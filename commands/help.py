from telegram.ext.dispatcher import run_async

@run_async
def handle(bot,update,args):
    text = "Commands:\n"
    text += "/price Partial Card Name - gives the price for a card. You can input a part of its name\n"
    text += "/help - displays this text\n"
    text += "/searchtxt Card Rule Text - gives a list of all cards which satisfies the search's criteria. The rule text must be in english\n"
    text += "Inline query:\n"
    text += "@mtgpictures_bot Partial Card Name - provides pictures for a card, you can input a part of its name\n"
    text += "Other:\n"
    text += "[[Card Name]] - can be used anywhere in text messages, sends you the card image"
    bot.send_message(chat_id=update.message.chat_id, text=text)
