from telegram.ext.dispatcher import run_async
from commutils.broadcast import send_random

@run_async
def handle(bot,update,args):
    #url=tracker.shorten(update,'https://telegram.me/storebot?start=mtgpictures_bot')
    url='https://telegram.me/storebot?start=mtgpictures_bot'
    if (len(args)==0):
        n = 10
    else:
        n = int(args[0])
    send_random(bot,'Rate me, if you please!\n'+url,n)
