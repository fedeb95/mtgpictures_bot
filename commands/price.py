import re
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup

from commutils.markupbuilders.mkm_builder import MkmBuilder
from providers import providers_manager
import tracker


COMMAND_NAME = 'price'

stored_response = dict([])
last_message= dict([])
builders = dict([])

@run_async
def handle_callback(bot,update):
    query = update.callback_query
    query.data = re.sub(COMMAND_NAME+'_', '', query.data)
    builder = builders[query.message.chat_id]
    if (query.data == "next"):
        builder.increment()
        builder.reply_with_markup(bot,query,stored_response,COMMAND_NAME)
    elif query.data == "prev":
        builder.decrement()
        builder.reply_with_markup(bot,query,stored_response,COMMAND_NAME)
    elif query.data == "back":
        cards = stored_response[query.message.chat_id]    
        bot.delete_message(chat_id=query.message.chat_id,message_id=query.message.message_id)
        send_results(cards,query,bot)
    else:
        index = int(query.data)
        update = query
        text = answer_price(stored_response[update.message.chat_id][index])
        keyboard = []
        keyboard.append([InlineKeyboardButton("Back",callback_data=COMMAND_NAME+"_back")])
        bot.delete_message(chat_id=query.message.chat_id,message_id=last_message[query.message.chat_id])
        msg = bot.send_message(chat_id=query.message.chat_id,message_id=query.message.message_id,text=text,
                         reply_markup=InlineKeyboardMarkup(keyboard))
        update_last_message(msg)
        tracker.custom_track(update,COMMAND_NAME,stored_response[update.message.chat_id][index]["name"]["1"]["productName"])

def answer_price(product):
    name = product["name"]["1"]["productName"]
    price_guide = product["priceGuide"]
    expansion = product["expansion"]
    average_ever_sold = price_guide["SELL"]
    lowest_all_conditions = price_guide["LOW"] 
    lowest_ex = price_guide["LOWEX"] 
    lowest_foil = price_guide["LOWFOIL"]
    average = price_guide["AVG"]
    trend = price_guide["TREND"]
    text = name + "\n"
    text += expansion + ":\n"
    text += "Lowest: " + str(lowest_all_conditions) + "€\n"
    text += "Lowest ex or better: " + str(lowest_ex) + "€\n"
    text += "Lowest foil: " + str(lowest_foil) + "€\n"
    text += "Average ever: " + str(average_ever_sold) + "€\n"
    text += "Recent average: " + str(average) + "€\n" 
    text += "Trend: " + str(trend) + "€\n"
    text += "Prices from magiccardmarket.eu"
    return text 

@run_async
def handle(bot,update,args):
    global stored_response,builders
    if update.message.chat_id in last_message:
        try:
            bot.delete_message(chat_id=update.message.chat_id,message_id=last_message[update.message.chat_id])
        except Exception as e:
            print(e)
    builders[update.message.chat_id] = MkmBuilder()
    builder = builders[update.message.chat_id]
    builder.add_field(["name","1","productName"])
    builder.add_field(["expansion"])
    provider = providers_manager.get(providers_manager.PRICE_PROVIDER)
    cardname = ' '.join(args)
    response = provider.products(name=cardname, game=1, language=1, match=0)
    response = response.json()["product"]
    if len(response) == 0:
        response = provider.products(name=cardname, game=1, language=5, match=0)
        response = response.json()["product"]
    if len(response) > 1:
        stored_response[update.message.chat_id] = response
        send_results(response, update,bot)
    elif len(response) == 0:
        bot.send_message(chat_id=update.message.chat_id, text="Card not found")
    else:
        tracker.custom_track(update,COMMAND_NAME,cardname)
        bot.send_message(chat_id=update.message.chat_id, text=answer_price(response[0]))

def send_results(response,update,bot):
    builder = builders[update.message.chat_id]
    reply_markup = builder.build(response,COMMAND_NAME)
    msg = bot.send_message(text='Select card and expansion:',
                           chat_id=update.message.chat_id,reply_markup=reply_markup)
    update_last_message(msg)

def update_last_message(msg):
    last_message[msg.chat_id] = msg.message_id
