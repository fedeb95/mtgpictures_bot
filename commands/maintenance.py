from telegram.ext.dispatcher import run_async
from commutils.authcheck import check_auth
from commutils.broadcast import send_broadcast

@run_async
def handle(bot,update,args):
    if(check_auth(update)):
            send_broadcast(bot,"Going down for maintenance, I'm back in a few minutes...")
