from mtgsdk.card import Card
import re
from telegram.ext.dispatcher import run_async

from commutils.markupbuilders.stringlist_builder import StringListBuilder
from providers import providers_manager
import tracker
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

COMMAND_NAME = 'searchtxt'

stored_response = dict([])
last_message= dict([])
builders = dict([])

@run_async
def handle_callback(bot, update):
    query = update.callback_query
    query.data = re.sub(COMMAND_NAME+'_', '', query.data)
    builder = builders[query.message.chat_id]
    if (query.data == "next"):
        builder.increment()
        builder.reply_with_markup(bot,query,stored_response,COMMAND_NAME)
    elif query.data == "prev":
        builder.decrement()
        builder.reply_with_markup(bot,query,stored_response,COMMAND_NAME)
    elif query.data == "back":
        cards = stored_response[query.message.chat_id]    
        bot.delete_message(chat_id=query.message.chat_id,message_id=query.message.message_id)
        send_results(cards,query,bot)
    else:
        index = int(query.data)
        update = query
        text = stored_response[update.message.chat_id][index]
        cards = providers_manager.get(providers_manager.CARD_PROVIDER).get_card(text) 
        for card in cards:
            if card.image_url != None:
                keyboard = []
                keyboard.append([InlineKeyboardButton("Back",callback_data=COMMAND_NAME+"_back")])
                bot.delete_message(chat_id=query.message.chat_id,message_id=last_message[query.message.chat_id])
                msg = bot.sendPhoto(chat_id=update.message.chat_id, photo=card.image_url,reply_markup=InlineKeyboardMarkup(keyboard))
                update_last_message(msg)
                #bot.edit_message_text(chat_id=query.message.chat_id,message_id=query.message.message_id,
                #        text=card.name,reply_markup=[])
                break

@run_async
def handle(bot,update,args):
    global stored_response
    tracker.custom_track(update, COMMAND_NAME)
    if update.message.chat_id in last_message:
        try:
            bot.delete_message(chat_id=update.message.chat_id,message_id=last_message[update.message.chat_id])
        except Exception as e:
            print(e)
    builders[update.message.chat_id] = StringListBuilder()
#        bot.send_message(chat_id=update.message.chat_id,text="Sorry, only one search by text at a time")
#        return
    cardrulestext = ' '.join(args)
    provider = providers_manager.get(providers_manager.FULLTEXT_PROVIDER)
    response = provider.search(cardrulestext) 
    if response == []:
        msg = bot.send_message(chat_id=update.message.chat_id, text="No cards found!")
        update_last_message(msg)
    else:
        cards_wout_duplicates = [c for c in list(set(response))]
        send_results(cards_wout_duplicates,update,bot)
        stored_response[update.message.chat_id] = cards_wout_duplicates

def update_last_message(msg):
    last_message[msg.chat_id] = msg.message_id

def send_results(cards,update,bot):
    builder = builders[update.message.chat_id]
    builder.add_field(["name"])
    reply_markup = builder.build(cards,COMMAND_NAME)
    msg = bot.send_message(text='Select a card from the list or press "Next >>" to see other cards:', 
                           chat_id=update.message.chat_id,reply_markup=reply_markup) #response
    update_last_message(msg)
