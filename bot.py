#!/usr/bin/python3
# encoding: utf-8

# telegram imports
import logging
import os
from sys import argv
from telegram.ext import InlineQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater
from providers import providers_manager 

from command_factory import CommandFactory
from config_manager import ConfigManager
from dbconn.postgres_manager import PostgresManager
from providers import providers_manager 
from providers.card_prov_mtgutils import CardProviderMtgutils
from providers.price_prov_mkm import PriceProviderMkm
from txtparser import Parser
from providers.fulltext_prov_mongo import FullTextProviderMongo


# other imports
commands_dir = "commands"

def unknown_cmd(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Command not found. Type /help for available commands")

# bot setup and start
def main():
    path = os.path.dirname(os.path.abspath(__file__))
    if (len(argv) != 2):
        print("Wrong number of arguments. Please provide config file.")
        return
    manager = ConfigManager.get_instance(path+"/"+argv[1])
    conn_m = PostgresManager.get_instance(manager.config["connection"])
    os.environ["MKM_APP_TOKEN"] = manager.config["mkm-app-token"]
    os.environ["MKM_APP_SECRET"] = manager.config["mkm-app-secret"]
    os.environ["MKM_ACCESS_TOKEN"] = ""
    os.environ["MKM_ACCESS_TOKEN_SECRET"] = ""
    
    providers_manager.register(providers_manager.PRICE_PROVIDER, PriceProviderMkm())
    providers_manager.register(providers_manager.CARD_PROVIDER, CardProviderMtgutils())
    providers_manager.register(providers_manager.FULLTEXT_PROVIDER, FullTextProviderMongo())

    print("starting bot...")
    parser = Parser()
    updater = Updater(token=manager.config["telegram"], workers=8)
    dispatcher = updater.dispatcher
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)
    text_handler = MessageHandler(Filters.text, parser.parseMessage)
    inline_handler = InlineQueryHandler(parser.parseInline)
    dispatcher.add_handler(text_handler)
    dispatcher.add_handler(inline_handler)

    factory = CommandFactory() 
    factory.create(commands_dir) 
    for handler in factory.handlers:
        dispatcher.add_handler(handler)

    updater.start_polling()
    print("bot started.")

#########################

if __name__ == "__main__":
    main()

