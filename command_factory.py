import importlib
import os
from telegram.ext import CommandHandler, CallbackQueryHandler
import re
from telegram.ext.conversationhandler import ConversationHandler

class CommandFactory:
    def __init__(self):
        self.handlers = []

    def create(self, dir_name):
        for file_name in os.listdir(dir_name):
            if not file_name.startswith("__") and file_name.endswith(".py"):
                module_name = re.sub('.py','',file_name)
                module = importlib.import_module("commands."+module_name)
                handle_fun = getattr(module,"handle")
                handler = CommandHandler(module_name, handle_fun, pass_args=True)
                try:
                    handle_callback_fun = getattr(module, "handle_callback")
                    pattern = module_name + '_[a-z0-9]+'
                    handler_callback = CallbackQueryHandler(handle_callback_fun,pattern=pattern)
                    self.handlers.append(handler_callback)
                except AttributeError:
                    pass
                self.handlers.append(handler)
                
