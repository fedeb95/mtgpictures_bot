#!/usr/bin/python3
# encoding: utf-8

import io 
import json
import zipfile
import requests
from pymongo import MongoClient

def main():
    r = requests.get("https://mtgjson.com/json/AllCards.json.zip")
    with zipfile.ZipFile(io.BytesIO(r.content)) as archive:
        js = json.loads(archive.open("AllCards.json").read().decode('utf-8'))
    client = MongoClient() 
    client.drop_database('cards')
    db = client['cards']
    collection = db['all']
    for card in js:
        result = collection.insert(js[card]) 
    collection.create_index([('text','text')])

if __name__=="__main__":
    main()
