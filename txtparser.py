#!/usr/bin/python3
# encoding: utf-8

import re
import _thread
from telegram import InlineQueryResultPhoto
import tracker
from providers import providers_manager

class Parser():

    def __init__(self):
        self.pattern = re.compile('\[\[([^\[]+)\]\]')
        self.mtg = providers_manager.get(providers_manager.CARD_PROVIDER)

    def fetch_engita(self,card_name):
        cards = self.mtg.get_card(card_name) 
        if cards == []:
            cards = self.mtg.get_card_language('italian', card_name)
        return cards


    def fetch_and_send(self,bot, update, matches):
        for card_name in matches:
            cards = self.fetch_engita(card_name)
            tracker.custom_track(update,"text")
            found=False
            for card in cards:
                if card.name.upper()==card_name.upper():
                    if card.image_url != None:
                        found=True
                        bot.sendPhoto(chat_id=update.message.chat_id, photo=card.image_url)
                        break
            if not found:
                bot.send_message(chat_id=update.message.chat_id, text="{} not found".format(card_name))

    def reply_inline(self,bot,update):
        query = update.inline_query.query
        if not query:
            return
        cards = self.fetch_engita(query)
        results = list()
        for card in cards:
            if card.image_url != None:
                results.append(
                    InlineQueryResultPhoto(
                        id=card.multiverse_id,
                        photo_url=card.image_url,
                        thumb_url=card.image_url,
                        title=card.name
                    )      
                )
        bot.answer_inline_query(update.inline_query.id, results)

    def parseMessage(self, bot, update):
        matches = self.pattern.findall(update.message.text)
        matches = list(set([s.strip() for s in matches]))
        if matches != []:
            #tracker.track(update,"text message")
            #tracker.custom_track(update,"text")
            _thread.start_new_thread(self.fetch_and_send, (bot, update, matches,))

    def parseInline(self, bot, update):
        #tracker.track(update,"inline image")
        _thread.start_new_thread(self.reply_inline, (bot,update,))    
        try:
            tracker.custom_track(update,"inline",update.inline_query.query)
        except:
            pass
