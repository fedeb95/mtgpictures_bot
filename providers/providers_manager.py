PRICE_PROVIDER = "PriceProvider"
CARD_PROVIDER = "CardProvider"
FULLTEXT_PROVIDER = "FullTextProvider"

providers = dict([])

def register(name, provider):
    providers[name] = provider

def get(name):
    return providers[name]
