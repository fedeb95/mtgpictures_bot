from mkmsdk.mkm import mkm 
from providers.price_prov import PriceProvider

class PriceProviderMkm(PriceProvider):
    def products(self, name, game, language, match):
        return mkm.market_place.products(name=name, game=game, language=language, match=match) 
