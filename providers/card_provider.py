'''
Created on 23 set 2017

@author: federico
'''

class CardProvider():
    '''
    classdocs
    '''

    def get_card(self, card_name):
        raise NotImplementedError("This class must be extended with an actual provider.")
        
    def get_card_language(self, language, card_name):
        raise NotImplementedError("This class must be extended with an actual provider.")