'''
Created on 09 ott 2017

@author: federico
'''
from providers.fulltext_provider import FullTextProvider
from pymongo.mongo_client import MongoClient
from config_manager import ConfigManager

class FullTextProviderMongo(FullTextProvider):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.client = MongoClient(ConfigManager.get_instance().config['mongo'])
        self.db = self.client.cards
        self.cards = self.db.all
        
    def search(self, text):
        words = text.split(' ')
        words = list(map(lambda s: '\"{0}\"'.format(s),words))
        query = " ".join(words)
        print(query)
        # TODO modify so that is more safe then just ++
        res = self.cards.find({'$text':{'$search':query, 
                                        '$caseSensitive':False,'$diacriticSensitive':False}},
                              {'score': {'$meta': 'textScore'}}).sort([('score', {'$meta': 'textScore'})]) 
        to_ret = []
        for card in res:
            to_ret.append(card['name'])
        return to_ret
