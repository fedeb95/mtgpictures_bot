'''
Created on 23 set 2017

@author: federico
'''
from providers.card_provider import CardProvider
from mtgutils import Mtg


class CardProviderMtgutils(CardProvider):
    '''
    classdocs
    '''
    
    def __init__(self):
        self.mtg = Mtg()

    def get_card(self, card_name):
        return self.mtg.get_card(card_name) 
        
    def get_card_language(self, language, card_name):
        return self.mtg.get_card_language(language, card_name)