class ConnManager():

    instance=None

    def __init__(self,conn_string):
        raise NotImplementedError()

    @staticmethod
    def get_instance(conn_string=None):
        if ConnManager.instance == None:
            raise NotImplementedError()
        return ConnManager.instance

    def get_conn(self):
        raise NotImplementedError()

    def get_cursor(self,conn):
        raise NotImplementedError()
    
    def commit(self,conn):
        raise NotImplementedError()
    
    def close(self,conn):
        raise NotImplementedError()
    
