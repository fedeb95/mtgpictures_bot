import psycopg2
from dbconn.conn_manager import ConnManager

class PostgresManager(ConnManager):

    def __init__(self,conn_string):
        self.conn_string = conn_string 

    @staticmethod
    def get_instance(conn_string=None):
        if ConnManager.instance == None: 
            if conn_string == None:
                raise ValueError("Connection string not provided!")
            ConnManager.instance = PostgresManager(conn_string)
        return ConnManager.instance
    
    def get_conn(self):
        conn = psycopg2.connect(self.conn_string)
        return conn

    def get_cursor(self,conn):
        return conn.cursor()

    def commit(self,conn):
        return conn.commit()

    def close(self,conn):
        conn.close()
        del conn
