import unittest
from test.fake_update import FakeUpdate
from commutils.authcheck import allowed, check_auth

class TestAuthcheck(unittest.TestCase):

    def test_check_auth_authorized(self):
        update = FakeUpdate(allowed)
        self.assertTrue(check_auth(update))
        
    def test_check_auth_not_authorized(self):
        update = FakeUpdate('not allowed user')
        self.assertFalse(check_auth(update))

if __name__=='__main__':
    unittest.main()
