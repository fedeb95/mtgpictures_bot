import unittest
from test.fake_conn_manager import FakeConnManager
from test.fake_bot import FakeBot
from commutils.broadcast import send_broadcast

class TestBroadcast(unittest.TestCase):

    def test_send_broadcast(self):
        message = "Hello World!"
        manager = FakeConnManager.get_instance("",3,2)
        bot = FakeBot()
        send_broadcast(bot, message)
        for msg in bot.messages:
            self.assertEqual(message,msg.text)
        for i in range(0,len(bot.chat_id)):
            self.assertEqual(manager.get_cursor("").dataset[i][1], bot.chat_id[i])
            
if __name__=='__main__':
    unittest.main()
