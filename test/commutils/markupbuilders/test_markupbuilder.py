import unittest
from commutils.markupbuilders.stringlist_builder import StringListBuilder

class TestAuthcheck(unittest.TestCase):
    def setUp(self):
        self.builder = StringListBuilder()
        self.source = ["element1","element2","element3"]

    def tearDown(self):
        del self.builder
        del self.source

    def test_build(self):
        keyboard = self.builder.build(self.source,"command")
        keyboard = keyboard.inline_keyboard
        self.assert_keyboard(keyboard,len(keyboard))

    def test_build_more_than_max(self):
        self.builder.max = 2
        keyboard = self.builder.build(self.source,"command")
        keyboard = keyboard.inline_keyboard
        self.assert_keyboard(keyboard,2)
        self.assertEqual("Next >>",keyboard[2][0].text)
        self.assertEqual("command_next",keyboard[2][0].callback_data)

    def assert_keyboard(self,keyboard,n):
        for i in range(0,n):
            self.assertEqual(self.source[i],keyboard[i][0].text)
            self.assertEqual("command_"+str(i),keyboard[i][0].callback_data)

if __name__=="__mail__":
    unittest.main()
