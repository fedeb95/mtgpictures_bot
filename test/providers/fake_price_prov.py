'''
Created on 21 set 2017

@author: federico
'''
from symbol import except_clause
import re
from providers.price_prov import PriceProvider


class FakePriceProvider(PriceProvider):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        
    def products(self, name, game, language, match):
        prod = self.fake_market_place[game][language];
        if match == 1:
            try:
                return prod[name]
            except KeyError:
                return []
        else:
            res = []
            for key in prod.keys:
                if re.match(name+"[a-zA-Z]*", key):
                    res.append(prod[key])
            return res