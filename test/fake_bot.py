from test.fake_update import FakeMessage
 
TEXT = "text"
REPLY_MARKUP = "reply_markup"

class FakeBot():
   
    def __init__(self):
        self.messages = [] 
        self.chat_id = []

    def send_message(self,chat_id=None, text=None, reply_markup=None):
        msg = FakeMessage()
        msg.text = text
        msg.reply_markup = reply_markup
        self.messages.append(msg)
        self.chat_id.append(chat_id)

        