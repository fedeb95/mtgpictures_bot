from dbconn.conn_manager import ConnManager
import uuid

def random_db_int(rownum,colnum):
    return [[i*j+i+2 for i in range(0,colnum)]for j in range(0,rownum)]

class FakeCursor():
    
    def __init__(self,rownum,colnum):
        self.query = ""
        self.params = ""
        self.dataset = random_db_int(rownum,colnum)
    
    def execute(self,query,params=[]):
        self.query = query
        self.params = params

    def fetchall(self):
        return self.dataset
        
class FakeConnManager(ConnManager):

    def __init__(self,conn_string):
        pass

    def init2(self,rownum,colnum):
        self.cursor = FakeCursor(rownum,colnum)
        self.committed = 0 

    @staticmethod
    def get_instance(conn_string=None,rownum=None,colnum=None):
        if ConnManager.instance == None: 
            ConnManager.instance = FakeConnManager(conn_string)
            ConnManager.instance.init2(rownum,colnum)
        return ConnManager.instance
    
    def get_conn(self):
        return ""

    def get_cursor(self,conn):
        return self.cursor

    def commit(self):
        self.committed += 1

    def close(self,conn):
        pass