#!/usr/bin/python3
# encoding: utf-8

from mtgsdk import Card

class Mtg:
    
    def __init__(self):
        self.quote = '"'
    
    def get_card(self,cardName):
        return Card.where(name=cardName).all()

    def get_card_language(self,language, cardName):
        return Card.where(language=language).where(name=self.quote+cardName+self.quote).all()
