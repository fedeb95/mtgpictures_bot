from telegram import InlineKeyboardButton, InlineKeyboardMarkup
MAX = 4             
KEY_NEXT = "Next >>"
KEY_PREV = "<< Prev"

class MarkUpBuilder():
    '''
    Abstract builder for a reply_markup. 
    Override build_fields to suite your needs.
    If n is the number od buttons and n > MAX,
    two buttons previous and next are appended to the
    keyboard, and the callback data are command_name+"_prev" and command_name+"_next"
    '''
    
    def __init__(self):
        self.fields = []
        self.index = 0
        self.max = MAX
    
    def add_field(self,field):
        self.fields.append(field)
        return self
    
    def set_fields(self, lst):
        self.fields = lst

    def build(self,entries,command_name):
        keyboard = []
        sup = self.index+self.max
        if self.index+self.max > len(entries):
            sup = len(entries)
        for i in range(self.index,sup):
            text = self.build_fields(entries[i]) 
            keyboard.append([InlineKeyboardButton(text,callback_data=command_name+'_'+str(i))])
        prev_button = InlineKeyboardButton(KEY_PREV,callback_data=command_name+"_prev")
        if self.index+self.max < len(entries):
            button_list = []
            if self.index > 0:
                button_list.append(prev_button)
            button_list.append(InlineKeyboardButton(KEY_NEXT,callback_data=command_name+"_next"))
            keyboard.append(button_list)
        elif len(entries) > self.max:
            keyboard.append([prev_button])
        reply_markup = InlineKeyboardMarkup(keyboard)
        return reply_markup
    
    def build_fields(self,product):
        raise NotImplementedError("Implement this method according to the structure you want to build.")

    def increment(self):
        self.index += self.max
        
    def decrement(self):
        self.index -= self.max
        
    def reply_with_markup(self,bot,query,stored_response,COMMAND_NAME):
        reply_markup = self.build(stored_response[query.message.chat_id],COMMAND_NAME)
        bot.edit_message_text(chat_id=query.message.chat_id,message_id=query.message.message_id,
                              reply_markup=reply_markup,text=query.message.text)

