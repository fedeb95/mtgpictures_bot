from commutils.markupbuilders.markup_builder import MarkUpBuilder


class MkmBuilder(MarkUpBuilder):
    
    def build_fields(self,source):
        text = ''
        for field in self.fields:
            tmp = source
            for idx in field:
                tmp = tmp[idx] 
            text += tmp + ": "
        text = text[:-2]
        return text