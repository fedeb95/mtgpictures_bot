from dbconn.conn_manager import ConnManager
import commutils.queries as queries

def send_broadcast(bot,text):
    conn = None
    try:
        conn = ConnManager.get_instance().get_conn()
        c = ConnManager.get_instance().get_cursor(conn)
        c.execute(queries.query_all_users)
        for row in c.fetchall():
            bot.send_message(chat_id=int(row[1]), text=text)
    finally:
        if conn != None:
            ConnManager.get_instance().close(conn)

def send_random(bot,text,n):
    conn = None
    try:
        conn = ConnManager.get_instance().get_conn()
        c = ConnManager.get_instance().get_cursor(conn)
        c.execute(queries.query_random_users,[n])
        for row in c.fetchall():
            bot.send_message(chat_id=int(row[1]), text=text)
    finally:
        if conn != None:
            ConnManager.get_instance().close(conn)

